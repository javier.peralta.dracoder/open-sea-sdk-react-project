import {configureStore} from '@reduxjs/toolkit'
import orderSlice from "./reducers/orderSlice";
import ordersSlice from "./reducers/ordersSlice";
import paginationSlice from "./reducers/paginationSlice";
import loadingSlice from "./reducers/loadingSlice";
import orderTraitsSlice from "./reducers/orderTraitsSlice";
import myOrdersSlice from "./reducers/myOrdersSlice";
import assetSlice from "./reducers/assetSlice";
import assetsSlice from "./reducers/assetsSlice";
import categoriesSlice from "./reducers/categoriesSlice";
import ethPriceSlice from "./reducers/ethPriceSlice";
import eventsSlice from "./reducers/eventsSlice";

export default configureStore({
    reducer: {
        order: orderSlice,
        orders: ordersSlice,
        pagination: paginationSlice,
        loading: loadingSlice,
        orderTraits: orderTraitsSlice,
        myOrders: myOrdersSlice,
        assets: assetsSlice,
        asset: assetSlice,
        categories: categoriesSlice,
        ethPrice: ethPriceSlice,
        events: eventsSlice,
    },
})