import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    asset: {},
};

const assetSlice = createSlice({
    name: 'asset',
    initialState,
    reducers: {
        setAsset: (state, action) => {
            state.asset = action.payload;
        }
    }
});

export const {setAsset} = assetSlice.actions;

export default assetSlice.reducer;