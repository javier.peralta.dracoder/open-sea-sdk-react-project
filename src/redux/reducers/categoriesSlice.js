import {createSlice} from "@reduxjs/toolkit";
import {act} from "@testing-library/react";

const initialState = {
    categories: [],
    category: null,
    isLoading: false,
    assets: [],
};

const categoriesSlice = createSlice({
    name: 'categories',
    initialState,
    reducers: {
        setCategories: (state, action) => {
            state.categories = action.payload;
        },
        setLoading: (state, action) => {
            state.isLoading = action.payload;
        },
        setAssets: (state, action) => {
            if (state.assets[action.payload.collection] === undefined) {
                state.assets.push(action.payload.collection);
                state.assets[action.payload.collection] = action.payload.assets;
            }
            // state.assets[action.payload.collection] = action.payload.assets;
            // console.log("asdasdasd")
        }
    }
});

export const {setCategories, setLoading, setAssets} = categoriesSlice.actions;

export default categoriesSlice.reducer;