import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    items: [],
    count: 0
};

const ordersSlice = createSlice({
    name: 'orders',
    initialState,
    reducers: {
        setItems: (state, action) => {
            state.items = action.payload;
        }
    }
});

export const {setItems} = ordersSlice.actions;

export default ordersSlice.reducer;