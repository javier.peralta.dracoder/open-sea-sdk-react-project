import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    owner: null,
};

const myOrdersSlice = createSlice({
    name: 'myOrders',
    initialState,
    reducers: {
        setOwner: (state, action) => {
            state.owner = action.payload;
        }
    }
});

export const {setOwner} = myOrdersSlice.actions;

export default myOrdersSlice.reducer;