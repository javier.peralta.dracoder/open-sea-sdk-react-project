import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    traits: [],
    isLoading: true,
};

const orderTraitsSlice = createSlice({
    name: 'orderTraits',
    initialState,
    reducers: {
        setTraits: (state, action) => {
            state.traits = action.payload;
        },
        setLoading: (state, action) => {
            state.isLoading = action.payload;
        }
    }
});

export const {setTraits, setLoading} = orderTraitsSlice.actions;

export default orderTraitsSlice.reducer;