import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    ethPrice: null,
};

const ethPriceSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
        setEthPrice: (state, action) => {
            state.ethPrice = action.payload;
        },
    }
});

export const {setEthPrice} = ethPriceSlice.actions;

export default ethPriceSlice.reducer;