import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    count: 0,
    options: {
        maker: null,
        owner: null,
        side: 0,
        bundled: false,
        page: 1
    },
};

const paginationSlice = createSlice({
    name: 'pagination',
    initialState,
    reducers: {
        setCount: (state, action) => {
            state.count = action.payload;
        },
        setOptions: (state, action) => {
            Object.keys(action.payload).forEach((key)=>{
                state.options[key] =action.payload[key];
            })
        },
        nextPage: (state) => {
            state.options.page++;
        },
        prevPage: (state) => {
            if (state.options.page > 1) {
                state.options.page--;
            }
        },
    }
});

export const {setCount, setOptions, nextPage, prevPage} = paginationSlice.actions;

export default paginationSlice.reducer;