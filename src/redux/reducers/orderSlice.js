import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    selectedOrder: null,
    isProcessing: false,
    errorMessage: '',
    transactionHash: null,
};

const orderSlice = createSlice({
    name: 'order',
    initialState,
    reducers: {
        setSelected: (state, action) => {
            state.selectedOrder = action.payload
        },
        setProcessing: (state, action) => {
            state.isProcessing = action.payload;
        },
        setError: (state, action) => {
            state.errorMessage = action.payload;
        },
        setTransactionHash: (state, action) => {
            state.transactionHash = action.payload;
        },
    }
});

export const {setProcessing, setSelected, setError, setTransactionHash} = orderSlice.actions;

export default orderSlice.reducer;