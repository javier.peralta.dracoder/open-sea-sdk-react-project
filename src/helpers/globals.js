import Swal from "sweetalert2";

export let accountAddress = null;

export const scrollToTop = () => {
    window.scrollTo(0, 0);
}

export const throttleError = () => {
    Swal.fire({
        title: 'Tiempo excedido',
        html: 'Tiempo de consulta excedido. Por favor intentelo nuevamente.<br><b class="text-info">Éste mensaje es únicamente a modo de desarrollo.</b>',
        confirmButtonText: 'Volver a intentarlo',
    }).then((result) => {
        window.location.reload(false);
    })
}

export const loadingList = () => {
    Swal.fire({
        title: 'Cargando contenido',
        html: '<i class="fas fa-spinner fa-spin"/>',
        showConfirmButton: false,
    });
}