import * as Web3 from "web3";
import {PortisProvider} from 'portis';
import {providerHost} from "./constants";
export let web3Provider = typeof web3 !== 'undefined'
    ? window.web3.currentProvider
    : new Web3.providers.HttpProvider(providerHost)

const networkCallbacks = [];

export async function connectWallet() {
    if (!window.web3) {
        web3Provider = new PortisProvider({
            // Put your Portis API key here
        })
    } else if (window.ethereum) {
        return window.ethereum.enable();
    } else {
        const errorMessage = 'You need an Ethereum wallet to interact with this marketplace. Unlock your wallet, get MetaMask.io or Portis on desktop, or get Trust Wallet or Coinbase Wallet on mobile.'
        alert(errorMessage)
        throw new Error(errorMessage)
    }

    networkCallbacks.map((c) => c(web3Provider))
}