import axios from "axios";
import {collectionsUri} from "../openSeaApiUris";
import {defaultOwner} from "../constants";

export const fetchCollections = async () => {
    return await axios.get(`${collectionsUri}?owner=${defaultOwner}&order_direction=desc&offset=0&limit=20`);
}
