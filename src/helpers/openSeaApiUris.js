export const baseUri = 'https://rinkeby-api.opensea.io/api/v1';
// export const baseUri = 'https://api.opensea.io/api/v1';

export const assetsUri = `${baseUri}/assets`;
export const bundlesUri = `${baseUri}/bundles`;
export const assetUri = `${baseUri}/asset`;
export const assetContractUri = `${baseUri}/asset_contract`;
export const eventsUri = `${baseUri}/events`;
export const collectionsUri = `${baseUri}/collections`;