import axios from "axios";
import {registerUserUri} from "../apiUris";

export const storeUser = ({user}) => {
    const {name, username, email, password} = user;

    return axios({
        method: 'post',
        url: registerUserUri,
        data: {
            name,
            username,
            email,
            password
        }
    });
}