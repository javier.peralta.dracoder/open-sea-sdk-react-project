import {seaport} from "../constants";

export const findOrder = async ({contractAddress, tokenId}) => {
    return await seaport.api.getOrder({
        token_id: tokenId,
        asset_contract_address: contractAddress,
    });
}