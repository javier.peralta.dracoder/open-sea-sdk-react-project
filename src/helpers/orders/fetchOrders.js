import {defaultOwner, seaport} from "../constants";
import {OrderSide, SaleKind} from "opensea-js/lib/types";

export const fetchOrders = async (
    {
        maker = null,
        owner, side = 0,
        bundled = false,
        page = 1,
        token_id = null,
        is_english = false,
        limit = 20,
    }
) => {
    owner = owner ? owner : defaultOwner;

    const {orders, count} = await seaport.api.getOrders(
        {
            owner,
            side: OrderSide.Sell,
            limit
            // is_english
            // is_english:true,
        }
        , page);

    return {orders, count};
}