import {seaport} from "../constants";

export const findAsset = async ({tokenAddress, tokenId}) => {
    return await seaport.api.getAsset({tokenAddress, tokenId});
}