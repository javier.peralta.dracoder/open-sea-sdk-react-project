import {defaultOwner, seaport} from "../constants";
import {OrderSide} from "opensea-js/lib/types";

export const fetchAssets = async (
    {
        maker = null,
        owner, side = 0,
        bundled = false,
        page = 1,
        token_id = null
    }
) => {
    owner = owner ? owner : defaultOwner;

    const {assets, count} = await seaport.api.getAssets(
        {
            owner,
            side: OrderSide.Sell,
        }
        , page);

    return {assets, count};
}