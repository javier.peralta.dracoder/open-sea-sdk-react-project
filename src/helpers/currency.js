import BigNumber from "bignumber.js";

export const DEFAULT_DECIMALS = 18
export const EURO_SYMBOL = '€';

export function toUnitAmount(baseAmount, tokenContract = null) {
    const decimals = tokenContract && tokenContract.decimals != null
        ? tokenContract.decimals
        : DEFAULT_DECIMALS

    const amountBN = new BigNumber(baseAmount ? baseAmount.toString() : "0");

    return amountBN.div(new BigNumber(10).pow(decimals))
}

