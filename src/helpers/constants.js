import * as Web3 from "web3";
import {Network, OpenSeaPort} from "opensea-js";
let web3;
// Modern DApp Browsers
if (window.ethereum) {
    web3 = new Web3(window.ethereum);
    try {
        window.ethereum.enable().then(function() {
            // User has allowed account access to DApp...
        });
    } catch(e) {
        // User has denied account access to DApp...
    }
}
// Legacy DApp Browsers
else if (window.web3) {
    web3 = new Web3(window.web3.currentProvider);
}
// Non-DApp Browsers
else {
    alert('Necesitas instalar Metamask para utilizar el servicio !');
}


// Use https://mainnet.infura.io for production and https://rinkeby.infura.io/v3/<PROJECT ID> for testing with Rinkeby
const providerHost = 'https://rinkeby.infura.io/v3/d67c5b6f1b1c4c31be725c270c434a0e';

// const provider = new Web3.providers.HttpProvider('https://mainnet.infura.io');
const provider = new Web3.providers.HttpProvider(providerHost);
//On NetworkName use Network.main for production and Network.Rinkeby for testing purposes.
const seaport = new OpenSeaPort(
    //As provider use provider in production and window.ethereum for testing transactions.
    window.ethereum,
    {
        networkName: Network.Rinkeby
    }
);
// const defaultOwner = null;
// const defaultOwner = '0xe4513a7BCd98dE50Fe7984C82dAf5611beBa7170';
// const defaultOwner = '0xb787a2810265f24863b99158f94649418a524c41';
const defaultOwner = '0x44dc45fa0524911d34bf6b098cac77c01a666dc8';

export {provider, seaport, defaultOwner, providerHost};
