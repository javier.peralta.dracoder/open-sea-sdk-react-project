import React, {useEffect} from 'react';
import AppRouter from "./routers/AppRouter";
import {useDispatch} from "react-redux";
import {setEthPrice} from "./redux/reducers/ethPriceSlice";
import {getEthPriceNow} from "get-eth-price";
import moment from 'moment';
import 'moment/locale/es';

const MoviesApp = () => {

    moment().locale('es');
    const dispatch = useDispatch();

    useEffect(() => {
        getEthPriceNow()
            .then(data => {
                for (let propName in data) {
                    if (data.hasOwnProperty(propName)) {
                        let propValue = data[propName];
                        dispatch(setEthPrice(propValue.ETH));
                    }
                }
            });

    }, [dispatch])

    return (
        <>
            <AppRouter/>
        </>
    );
};

export default MoviesApp;