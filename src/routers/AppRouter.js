import React from 'react';
import {
    BrowserRouter as Router, Redirect, Route,
    Switch,
} from "react-router-dom";
import NavBar from "../components/ui/NavBar";
import ListScreen from "../components/orders/ListScreen";
import AboutScreen from "../components/about/AboutScreen";
import ContactScreen from "../components/contact/ContactScreen";
import MyResourcesScreen from "../components/my_resources/MyResourcesScreen";
import MyResourceDetailScreen from "../components/my_resources/MyResourceDetailScreen";
import CollectionsListScreen from "../components/collections/CollectionsListScreen";
import AssetDetailsScreen from "../components/order/AssetDetailScreen";
import RegisterScreen from "../components/users/RegisterScreen";

const AppRouter = () => {
    return (
        <>
            <Router>

                <NavBar/>

                <div className={'container-fluid'}>
                    <Switch>
                        <Route exact path={'/mis-recursos'} component={MyResourcesScreen}/>
                        <Route exact path={'/mis-recursos/:tokenAddress/:tokenId'}
                               component={MyResourceDetailScreen}/>
                        <Route exact path={'/recurso/:contractAddress/:tokenId'} component={AssetDetailsScreen}/>
                        <Route exact path={'/nosotros'} component={AboutScreen}/>
                        <Route exact path={'/contacto'} component={ContactScreen}/>
                        <Route exact path={'/categorias/:collectionSlug'} component={CollectionsListScreen}/>
                        <Route exact path={'/ofertas'} render={(props) => <ListScreen {...props} auction={true}/>}/>

                        <Route exact path={'/usuarios/registro'} component={RegisterScreen}/>

                        <Route exact path={'/'} component={ListScreen}/>

                        <Redirect to={'/'}/>
                    </Switch>
                </div>
            </Router>
        </>
    );
};

export default AppRouter;