import * as Web3 from 'web3'
import {OpenSeaPort, Network} from 'opensea-js'
import './App.css';

const provider = new Web3.providers.HttpProvider('https://mainnet.infura.io')
const seaport = new OpenSeaPort(provider, {
    networkName: Network.Main
});
let assets = null;

const owner = '0xe4513a7bcd98de50fe7984c82daf5611beba7170';

function App() {

    seaport.api.getAssets({
        owner
    })
        .then((data) => {
            assets = data;
        })
        .catch(error => {
            console.log(error)
        });
    return (
        <>
            {assets?.length}
        </>
    );
}

export default App;
