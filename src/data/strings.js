export const orderProcessingAlert = {
    title: 'Procesando',
    message: `
        <i class="fas fa-spinner fa-spin fa-2x"></i>
        <br><br>
        <p>Tu orden se está procesando. Por favor aguarda un momento...</p>
        <ol class="text-left">
            <li>Debes firmar la transacción.</li>
            <li>Una vez firmada esperar un momento a que impacten los cambios.</li>
            <li>Una vez finalizada la compra, puede el recurso puede demorar unos minutos en aparecer en tu colección.</li>
            <li>Para ver mas detalles sobre el estado de tu transacción visita <a href="https://rinkeby.etherscan.io/" target="_blank">Etherscan</a></li>
        </ol>
`,
}

export const orderProcessedAlert = {
    title: '¡Éxito!',
    message: `Tu orden se procesó con éxito.`,
};

export const loadingList = {
    title:'Cargando...',
    message:'Cargando...',
}
