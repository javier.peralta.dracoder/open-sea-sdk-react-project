import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setCount, setOptions} from "../../redux/reducers/paginationSlice";
import {setLoading} from "../../redux/reducers/loadingSlice";
import {fetchOrders} from "../../helpers/orders/fetchOrders";
import {scrollToTop, throttleError} from "../../helpers/globals";
import {toast} from "react-toastify";
import {orderProcessedAlert} from "../../data/strings";
import {setTransactionHash} from "../../redux/reducers/orderSlice";
import Card from "../orders/Card";
import CardLoading from "../orders/CardLoading";
import axios from "axios";
import {assetsUri} from "../../helpers/openSeaApiUris";
import {defaultOwner} from "../../helpers/constants";
import {OrderSide} from "opensea-js/lib/types";
import AssetCardSdk from "../orders/AssetCardSdk";
import AssetCard from "../orders/AssetCard";

const RelatedAssets = () => {

    const dispatch = useDispatch();
    const {isLoading} = useSelector(state => state.loading);
    const [assets, setAssets] = useState(null);

    dispatch(setOptions({is_english: false, limit: 4}));

    const getAssets = () => {
        axios.get(`${assetsUri}`, {
            params: {
                owner: defaultOwner,
                limit: 4,
                side: OrderSide.Buy,
            }
        }).then(({data}) => {
            setAssets(data.assets);
            dispatch(setLoading(false));
        }).catch(error => {
            getAssets();
        })
    }
    useEffect(() => {
        dispatch(setLoading(true));

        setTimeout(function () {
            getAssets();
        }, 4000);

    }, [dispatch])

    return (
        <>
            {
                !isLoading &&
                assets?.map((asset, index) => (
                    <div key={index} className={'col-12 col-sm-6 col-md-3 col-xl-3'}>
                        <AssetCard key={index} asset={asset}/>
                    </div>
                ))
            }

            {isLoading === true && <CardLoading count={4}/>}
        </>
    );
};

export default RelatedAssets;