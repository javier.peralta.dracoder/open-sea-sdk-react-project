import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setCount, setOptions} from "../../redux/reducers/paginationSlice";
import {setLoading} from "../../redux/reducers/loadingSlice";
import {fetchOrders} from "../../helpers/orders/fetchOrders";
import {scrollToTop, throttleError} from "../../helpers/globals";
import {toast} from "react-toastify";
import {orderProcessedAlert} from "../../data/strings";
import {setTransactionHash} from "../../redux/reducers/orderSlice";
import Card from "../orders/Card";
import CardLoading from "../orders/CardLoading";

const Related = () => {

    const dispatch = useDispatch();
    const [orders, setOrders] = useState(null);
    const {isLoading} = useSelector(state => state.loading);

    dispatch(setOptions({is_english: false, limit: 4}));

    useEffect(() => {
        dispatch(setLoading(true));
        setTimeout(function () {
            fetchOrders({is_english: false, limit: 4})
                .then(({orders, count}) => {
                    setOrders(orders);
                    dispatch(setCount(count));
                    dispatch(setLoading(false));
                })
                .catch(() => {
                    throttleError();
                });
        }, 4000);

    }, [dispatch])

    return (
        <>
            {
                !isLoading &&
                orders?.map((order, index) => (
                    <div key={index} className={'col-12 col-sm-6 col-md-3 col-xl-3'}>
                        <Card key={index} order={order}/>
                    </div>
                ))
            }

            {isLoading === true && <CardLoading count={4}/>}
        </>
    );
};

export default Related;