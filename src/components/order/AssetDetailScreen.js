import React, {useEffect, useState} from 'react';
import {useParams} from "react-router";
import {findOrder} from "../../helpers/orders/findOrder";
import BuyButton from "../orders/BuyButton";
import 'reactjs-popup/dist/index.css';
import Traits from "./Traits";
import {useDispatch, useSelector} from "react-redux";
import {setLoading, setTraits} from "../../redux/reducers/orderTraitsSlice";
import Swal from "sweetalert2";
import {assetUri, eventsUri} from "../../helpers/openSeaApiUris";
import axios from "axios";
import {setAsset} from "../../redux/reducers/assetSlice";
import Skeleton from "react-loading-skeleton";
import Breadcrumbs from "../ui/Breadcrumbs";
import SalePrice from "../orders/SalePrice";
import PushButton from "../orders/PushButton";
import PriceHistory from "./PriceHistory";
import {setEvents} from "../../redux/reducers/eventsSlice";
import RelatedAssets from "./RelatedAssets";

const AssetDetailsScreen = ({history}) => {
    const dispatch = useDispatch();
    const {contractAddress, tokenId} = useParams();

    if (!tokenId) {
        history.goBack();
    }

    const [order, setOrder] = useState({});
    const {traits} = useSelector(state => state.orderTraits);
    const {asset} = useSelector(state => state.asset);
    const {events} = useSelector(state => state.events);


    const getOrder = () => {
        setTimeout(function () {
            findOrder(contractAddress, tokenId)
                .then(order => {
                    setOrder(order);
                })
                .catch(() => {
                    setOrder(null);
                })

        }, 5000);
    }

    const getAsset = (assetContractAddress, tokenId) => {
        dispatch(setLoading(true));
        axios
            .get(`${assetUri}/${assetContractAddress}/${tokenId}`)
            .then(({data}) => {
                dispatch(setAsset(data));
                dispatch(setTraits(data.traits));
                dispatch(setLoading(false));
                getEvents(assetContractAddress);
                getOrder();
            })
            .catch(error => {
                getAsset(assetContractAddress, tokenId)
            });
    }

    const getEvents = (assetContractAddress) => {
        setTimeout(function () {
            axios.get(`${eventsUri}`, {
                params: {
                    asset_contract_address: assetContractAddress,
                    token_id: tokenId,
                }
            })
                .then(({data}) => {
                    dispatch(setEvents(data.asset_events));
                })
                .catch(error => {
                    getEvents(assetContractAddress);
                })
        }, 2000);

    }

    useEffect(() => {
        getAsset(contractAddress, tokenId);
    }, [dispatch, history, tokenId, contractAddress]);

    const imageModal = (imageUrl) => {
        Swal.fire({
            title: order?.asset?.name,
            text: order?.asset?.description,
            imageUrl,
            confirmButtonText: 'Cerrar',
        });
    }

    const videoModal = (video) => {
        Swal.fire({
            width: '80vw',
            html: `<video  loop autoPlay='' width="100%">
                    <source src="${video}" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>`,
            confirmButtonText: 'Cerrar',
        })
    }

    const renderVideo = (video) => {
        return (
            <>
                <video loop autoPlay={true} width={'100%'} className={'btn img-thumbnail'} muted
                       onClick={() => {
                           videoModal(video);
                       }}
                >
                    <source src={asset?.animation_original_url} type="video/mp4"/>
                </video>
            </>
        )
    }

    const unlockableContentalert = () => {
        Swal.fire({
            title: 'Contenido desbloqueable',
            html: '<p><b>Solamente el dueño de éste recurso puede visualizar el contenido desbloqueable.</b></p>',
            confirmButtonText: '¡Entendido!'
        })
    }

    return (
        <div className={'container asset_details__'}>
            <div className={'row mt-4'}>
                <Skeleton/>
                {
                    !asset?.name
                        ?
                        (
                            <>
                                <div className={'col-1'}>
                                    <Skeleton/>
                                </div>
                                <div className={'col-1'}>
                                    <Skeleton/>
                                </div>
                                <div className={'col-1'}>
                                    <Skeleton/>
                                </div>
                            </>
                        )
                        :
                        (<Breadcrumbs items={[
                            {
                                title: asset?.collection?.name,
                                path: `/categorias/${asset?.collection?.name}`,
                                active: false,
                            },
                            {
                                title: asset?.name,
                                path: '#',
                                active: true,
                            },
                        ]}/>)
                }


            </div>

            <div className={'row mt-4'}>
                <div className={'col-6 text-center asset_details__image'}>
                    {asset?.animation_original_url && renderVideo(asset?.animation_original_url)}
                    {
                        (!asset?.animation_original_url && asset?.image_url) &&
                        (<img src={asset?.image_url} alt={asset?.name}
                              className={'img-fluid btn asset_detail__image img-thumbnail'}
                              onClick={() => {
                                  imageModal(asset?.image_url);
                              }}
                        />)
                    }
                    {(!asset?.animation_original_url && !asset?.image_url) &&
                    <Skeleton height={200} className={'mb-3'}/>}

                </div>

                <div className={'col-6'}>
                    {!asset?.name && <Skeleton height={40}/>}
                    <h1 className={'pb-3 mb-3 lora-bold font-weight-bolder'}>{asset?.name}</h1>

                    <div className={'asset_details__info d-flex justify-content-between'}>
                        <span className={'asset_details__info-due-time work-sans-semibold text-dark-gray'}>Finaliza en 16 días</span>
                        <span className={'asset_details__info-due-date work-sans-regular text-medium-gray'}>26 de mayo de 2021 a las 18:30 hs</span>
                        <span className={'asset_details__info-visits work-sans-regular text-medium-gray'}>|&emsp;<i
                            className={'fas fa-eye'}></i> 185 visitas</span>
                    </div>

                    <article className={'border-top pt-3 mt-3 asset_details__description'}>
                        {!asset?.description && <Skeleton count={5}/>}
                        <p className={'work-sans-light'}>{asset?.description}</p>
                    </article>

                    <article className={'d-none'}>
                        <Traits traits={traits}/>
                    </article>

                    <div
                        className={'alert alert-primary btn btn-block box-shadow-7'}
                        onClick={() => {
                            unlockableContentalert();
                        }}
                    >
                        <p className={'text-center m-0'}>
                            <i className={'fas fa-lock'}/>
                            &emsp;<b className={' work-sans-light'}>Incluye contenido desbloqueable</b>
                        </p>
                    </div>
                    <div className={'price-container pt-0 pb-4 d-flex justify-content-around align-content-center'}>
                        {/*<Units order={order}/>*/}
                        <span className={'my-1 text-medium-gray d-flex flex-column text-center'}>
                            <span className={'float-left py-1 mr-2 work-sans-regular'}>Precio actual</span>
                            {/*<SalePriceAsset sellOrders={asset.sell_orders}/>*/}
                            <SalePrice order={order}/>
                            {/*    &emsp;*/}
                            {/*    <UsdPrice order={order}/>*/}
                        </span>

                        {order && <PushButton order={order} history={history}/>}

                        {order && <BuyButton order={order} history={history}/>}
                    </div>

                    {/*<div className={'asset_details__price-history bg-gray'}>*/}
                    <PriceHistory events={events}/>
                    {/*</div>*/}

                </div>

            </div>

            <div className={'container my-5'}>
                <p className={'text-center lora-regular'}>Productos relacionados</p>
            </div>

            <div className={'row my-4 justify-content-between'}>
                <RelatedAssets/>
            </div>
        </div>
    );
};


export default AssetDetailsScreen;