import React from 'react';

const Units = ({order}) => {
    return (
        <>
            <p className={'d-none'}>Unidades: <b>{order?.metadata?.asset?.quantity}</b></p>
        </>
    );
};


export default Units;