import React from 'react';
//
const Traits = ({traits}) => {
    return (
        <>
            {
                traits.length > 0 &&
                <ul className={'list-unstyled border-top border-bottom py-2'}>
                    {
                        traits.map((trait, index) => {
                            return <li key={index}>
                                {trait.trait_type}: <b>{trait.value} {trait.max_value && <>/ {trait.max_value}</>}</b>
                            </li>
                        })
                    }
                </ul>
            }
        </>
    );
}
;


export default Traits;