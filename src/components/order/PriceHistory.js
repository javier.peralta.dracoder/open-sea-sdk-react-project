import React from 'react';
import {EURO_SYMBOL, toUnitAmount} from "../../helpers/currency";
import Skeleton from "react-loading-skeleton";
import moment from "moment";
import {useSelector} from "react-redux";

const PriceHistory = ({events}) => {

    const {ethPrice} = useSelector(state => state.ethPrice);

    const price = (event) => {
        const price = toUnitAmount(event.bid_amount);
        const priceLabel = parseFloat(price).toLocaleString(undefined, {minimumSignificantDigits: 1});

        return Number(priceLabel);
    }

    events = events?.filter(event => event.bid_amount > 0);

    return (
        <div className={`asset_details__price-history bg-gray text-medium-gray ${!events?.length ? 'd-none' : ''}`}>
            <h4 className={'work-sans-regular'}>Histórico de precios</h4>
            <hr/>
            {!events && <Skeleton count={5}/>}
            {
                events &&
                (
                    <table className={'table text-center table-borderless text-medium-gray'}>
                        <tbody>
                        {events?.map((event, index) => {
                            if (price(event) > 0) {
                                return <tr key={index}>
                                    <td>{(price(event) * ethPrice.EUR).toFixed(2)}{EURO_SYMBOL}</td>
                                    <td>{event.from_account?.user?.username}</td>
                                    <td>{moment(event.created_date).fromNow()}</td>
                                </tr>
                            }
                        })}

                        </tbody>
                    </table>
                )
            }

        </div>
    );
};

PriceHistory.propTypes = {};

export default PriceHistory;