import React, {useCallback, useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import {useParams} from "react-router";
import axios from "axios";
import {assetsUri} from "../../helpers/openSeaApiUris";
import Aside from "../ui/Aside";
import Empty from "../orders/Empty";
import Pagination from "../orders/Pagination";
import CardLoading from "../orders/CardLoading";
import AssetCard from "../orders/AssetCard";
import {OrderSide} from "opensea-js/lib/types";

const CollectionsListScreen = () => {
    const dispatch = useDispatch();
    const {collectionSlug} = useParams();
    const [assets, setAssets] = useState(null);

    const getAssets = useCallback(() => {
            setTimeout(function () {
                axios.get(`${assetsUri}`, {
                    params: {
                        collection: collectionSlug,
                        side: OrderSide.Sell,
                    }
                }).then(({data}) => {
                    setAssets(data.assets);
                }).catch(error => {
                    getAssets();
                })
            }, 2000);
    },[collectionSlug])


    useEffect(() => {
        setAssets(null);
        getAssets();
    }, [dispatch, collectionSlug,getAssets]);

    return (
        <>
            <div className={'col-12 pt-5'}>
                <div className={'row mt-3 mb-5 mt-5 pt-5'}>
                    <div className={'col-sm-2'}>
                        <Aside/>
                    </div>

                    <div className={'col-sm-10 justify-content-center'}>
                        <h4 className={'text-center lora-regular mb-4'}>{collectionSlug}</h4>
                        <div className={'row'}>

                            {assets?.length && assets?.length >= 20 ? <Pagination/> : null}
                            {
                                assets?.map((asset, index) => (
                                    <div key={index} className={'p-1 col-12 col-sm-6 col-md-3 col-xl-3'}>
                                        <AssetCard key={index} asset={asset}/>
                                    </div>
                                ))
                            }

                            {assets && !assets?.length && <Empty/>}

                            {(!assets && !assets?.length) && <CardLoading count={8}/>}

                            {assets?.length && assets?.length >= 20 ? <Pagination/> : null}
                        </div>
                    </div>
                </div>

            </div>
        </>
    );
};

CollectionsListScreen.propTypes = {};

export default CollectionsListScreen;