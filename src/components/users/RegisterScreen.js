import React, {useState} from 'react';
import {useForm} from "react-hook-form";
import {storeUser} from "../../helpers/users/users";
import Swal from "sweetalert2";

const RegisterScreen = () => {

    const {register, handleSubmit, watch, formState: {errors}} = useForm();
    const [registering, setRegistering] = useState(false);

    const onSubmit = (data) => {
        if (data.password !== data.password_confirm) {
            Swal.fire('¡Error!', 'Las contraseñas deben coincidir', 'error');

            return false;
        }

        setRegistering(true);

        storeUser({user: data})
            .then(({data}) => {
                Swal.fire('', '¡Usuario creado con éxito!', 'success');
            })
            .catch(error => {
                Swal.fire('', 'Ocurrió un error al intentar crear el usuario. Inténtelo nuevamente.', 'error');

                setRegistering(false);
            });
    }

    return (
        <div className={'section-main'}>
            <div className={'user_form__'}>
                <div className={'user_form__card bg-gray p-4 box-shadow-7'}>
                    {
                        !registering ?
                            (
                                <>
                                    <h2 className={'lora-bold border-bottom pb-3 mb-4'}>Registro de usuario</h2>

                                    <form onSubmit={handleSubmit(onSubmit)}>
                                        <div className={'row mb-2'}>
                                            <div className={'col-12'}>
                                                <input
                                                    type={'text'}
                                                    name={'name'}
                                                    placeholder={'Nombre'}
                                                    className={'form-control mb-2'}
                                                    required
                                                    {...register('name')}
                                                />

                                                <input
                                                    type={'text'}
                                                    name={'username'}
                                                    placeholder={'Usuario'}
                                                    className={'form-control mb-2'}
                                                    required
                                                    {...register('username')}
                                                />

                                                <input
                                                    type={'email'}
                                                    name={'email'}
                                                    placeholder={'Email'}
                                                    className={'form-control mb-2'}
                                                    required
                                                    {...register('email')}
                                                />

                                                <input
                                                    type={'password'}
                                                    name={'password'}
                                                    placeholder={'Contraseña'}
                                                    className={'form-control mb-2'}
                                                    required
                                                    {...register('password')}
                                                />

                                                <input
                                                    type={'password'}
                                                    name={'password'}
                                                    placeholder={'Repetir Contraseña'}
                                                    className={'form-control mb-4'}
                                                    required
                                                    {...register('password_confirm')}
                                                />

                                                <div className={'text-center'}>
                                                    <button
                                                        type={'submit'}
                                                        className={'btn btn-primary w-50 py-3'}
                                                    >
                                                        Registro&emsp;<i className={'fas fa-check'}></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </div>

                                    </form>
                                </>
                            ) :
                            (
                                <div className={'text-center text-secondary p-5'}>
                                    <i className={'fas fa-spinner fa-spin fa-2x'}></i>
                                    <p className={'mt-2 lora-bold'}>Registrando</p>
                                </div>
                            )
                    }

                </div>
            </div>
        </div>
    );
};

export default RegisterScreen;