import React from 'react';

const ContactScreen = () => {
    return (
        <section className={'container text-center'}>
            <h1>Contacto</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur, dolorem labore minus
                necessitatibus officiis pariatur perferendis quisquam reprehenderit veritatis! Accusamus, aperiam
                corporis cupiditate dolorum minus nesciunt optio perspiciatis veritatis.</p>
        </section>
    );
};

export default ContactScreen;