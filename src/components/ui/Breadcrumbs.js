import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";

const Breadcrumbs = ({items}) => {
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
                <li key={99} className={`breadcrumb-item`} aria-current="page">
                    <NavLink to={'/'}>
                        Home
                    </NavLink>
                </li>
                {items.map((item, index) => (
                    <li key={index} className={`breadcrumb-item ${item.active ? 'active' : ''}`} aria-current="page">
                        <NavLink to={item.path}>
                            {item.title}
                        </NavLink>
                    </li>
                ))}
            </ol>
        </nav>
    );
};

Breadcrumbs.propTypes = {};

export default Breadcrumbs;