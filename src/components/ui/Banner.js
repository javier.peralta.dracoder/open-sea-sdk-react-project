import React from 'react';

const Banner = () => {
    return (
        <div className={'banner__'}>
            <div className="banner__img-background row justify-content-center align-content-center">
                <div className={'col-4 text-center'}>
                    <h2 className={'lora-bold font-weight-bolder'}>Lorem ipsum dolor sit amet,</h2>
                    <p className={'lora-regular'}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                        diam nonummy nibh euismod tincidunt ut laoreet dolore
                        magna aliquam erat volutpat.</p>
                </div>
            </div>
        </div>
    );
};

export default Banner;