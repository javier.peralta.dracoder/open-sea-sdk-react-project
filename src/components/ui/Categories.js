import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCollections} from "../../helpers/collections/fetchCollections";
import {setCategories, setLoading} from "../../redux/reducers/categoriesSlice";
import Skeleton from "react-loading-skeleton";
import {NavLink} from "react-router-dom";

const Categories = () => {

    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.categories);

    const collections = () => {
        setTimeout(function () {
            fetchCollections()
                .then((response) => {
                    dispatch(setCategories(response.data.collections));
                    dispatch(setLoading(false));
                })
        }, 2000);
    }

    useEffect(() => {
        collections();
    }, [dispatch]);

    return (
        <div className={'bg-gray categories__ px-3 py-5 mt-5'}>
            <h5 className={'categories__title mb-4'}>CATEGORÍAS</h5>
            {
                categories.length ?
                    <ul className={'list-unstyled'}>
                        {
                            categories?.map((category, index) => {
                                return <li key={index}>
                                    <NavLink to={`/categorias/${category.slug}`}>
                                        {category.name}
                                    </NavLink>
                                </li>
                            })
                        }
                    </ul>
                    :
                    <p><Skeleton count={5} duration={2}/></p>
            }
        </div>
    );
};

export default Categories;