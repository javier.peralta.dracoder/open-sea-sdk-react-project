import React from 'react';
import {Link, NavLink} from "react-router-dom";

const NavBar = () => {
    return (
        <header className={'bg-gray'}>
            <div className={'container'}>
                <nav className="navbar navbar-expand-lg navbar-light bg-light navbar__">

                    <Link className={'navbar-brand'} to="/">LOGO</Link>

                    <button className="navbar-toggler"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <NavLink
                                    activeClassName={'active'}
                                    className="nav-link"
                                    exact
                                    to={'/'}
                                >
                                    Home
                                </NavLink>
                            </li>

                            <li className="nav-item">
                                <NavLink
                                    activeClassName={'active'}
                                    className="nav-link"
                                    exact
                                    to={'/ofertas'}
                                >
                                    Ofertas
                                </NavLink>
                            </li>

                            <li className="nav-item">
                                <NavLink
                                    activeClassName={'active'}
                                    className="nav-link"
                                    exact
                                    to={'/nosotros'}
                                >
                                    Nosotros
                                </NavLink>
                            </li>

                            <li className="nav-item">
                                <NavLink
                                    activeClassName={'active'}
                                    className="nav-link"
                                    exact
                                    to={'/contacto'}
                                >
                                    Contacto
                                </NavLink>
                            </li>

                            <li className="nav-item">
                                <NavLink
                                    activeClassName={'active'}
                                    className="nav-link"
                                    exact
                                    to={'/mis-recursos'}
                                >
                                    Mis recursos
                                </NavLink>
                            </li>

                        </ul>
                        <form className="form-inline my-0 my-lg-0">
                            <div className="input-group mb-3">

                                <input type="search" className="form-control mr-sm-2 lora-regular navbar__search-box"
                                       placeholder="Buscar un recurso..." aria-label="Username"
                                       aria-describedby="basic-addon1"/>
                                <div className="input-group-append">
                            <span className="input-group-text btn navbar__search-icon" id="basic-addon1">
                                <i className={'fas fa-search'}></i>
                            </span>
                                </div>
                            </div>
                        </form>

                        <Link className={'btn navbar__user-button'} to={'/usuarios/registro'}>
                            <i className={'far fa-user'}></i>
                        </Link>
                    </div>
                </nav>
            </div>
        </header>
    );
};

export default NavBar;