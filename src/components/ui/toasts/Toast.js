import React from 'react';
import {ToastContainer} from "react-toastify";

const Toast = ({position='bottom-right',autoclose=5000,hideProgressbar=false}) => {
    return (
        <ToastContainer
            position={position}
            autoClose={autoclose}
            hideProgressBar={hideProgressbar}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
    );
};

export default Toast;