import React from 'react';

const AboutScreen = () => {

    return (
        <section className={'container text-center'}>
            <h1>Nosotros</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus adipisci alias, animi asperiores
                consequuntur corporis dolore eos exercitationem facilis impedit inventore magnam modi perspiciatis
                possimus provident quis totam voluptate!</p>

        </section>
    );
};

export default AboutScreen;