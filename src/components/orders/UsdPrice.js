import React from 'react';
import {toUnitAmount} from "../../helpers/currency";

const UsdPrice = ({order}) => {
    const {currentPrice, paymentTokenContract} = order;
    const price = toUnitAmount(currentPrice, paymentTokenContract);
    const usdPrice = parseFloat((order?.paymentTokenContract?.usdPrice * price)).toFixed(2);

    return (
        <>
            {order?.paymentTokenContract?.usdPrice &&
            <span className={'font-weight-lighter mt-1 ml-3 float-left'}>(USD {usdPrice})</span>}
        </>
    );
};

export default UsdPrice;