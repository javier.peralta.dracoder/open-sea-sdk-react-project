import React from 'react';
import Skeleton from "react-loading-skeleton";

const CardLoading = ({count = 1}) => {

    const items = [];

    for (let i = 0; i < count; i++) {
        items.push(
            <div key={i} className={'p-1 col-12 col-sm-6 col-md-3 col-xl-3'}>
                <div className={'card'} key={i}>
                    <div className="card-body">
                        <Skeleton height={200} className={'mb-3'}/>
                        <h3 className="card-title"><Skeleton/></h3>

                        <Skeleton height={40} className={'mb-4'}/>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <>
            {items}
        </>
    );
};

export default CardLoading;