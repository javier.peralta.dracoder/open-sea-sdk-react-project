import React from 'react';
import {toUnitAmount} from "../../helpers/currency";
import {useSelector} from "react-redux";
import Skeleton from "react-loading-skeleton";

const EuroPrice = ({order}) => {
    const {currentPrice, paymentTokenContract} = order;
    const price = toUnitAmount(currentPrice, paymentTokenContract);
    const priceLabel = parseFloat(price).toLocaleString(undefined, {minimumSignificantDigits: 1});
    const {ethPrice} = useSelector(state => state.ethPrice);

    return (
        <>
            <h4 className={'work-sans-bold font-weight-bolder'}>{!ethPrice ? <Skeleton/> : (priceLabel * ethPrice.EUR).toFixed(2)+"€"}</h4>
        </>
    );
};

export default EuroPrice;