import React, {useEffect} from 'react';
import {toUnitAmount} from "../../helpers/currency";
import {useSelector} from "react-redux";
import Skeleton from "react-loading-skeleton";

const SalePrice = ({order}) => {
    const {currentPrice, paymentTokenContract} = order;
    const price = toUnitAmount(currentPrice, paymentTokenContract);
    const priceLabel = parseFloat(price).toLocaleString(undefined, {minimumSignificantDigits: 1});
    const {ethPrice} = useSelector(state => state.ethPrice);

    return (
        <h5>
            {!ethPrice ? <Skeleton/> : (priceLabel * ethPrice.EUR).toFixed(2)+"€"}
            {/*{priceLabel} { paymentTokenContract ? paymentTokenContract.symbol : ''}*/}
        </h5>
    );
};

export default SalePrice;