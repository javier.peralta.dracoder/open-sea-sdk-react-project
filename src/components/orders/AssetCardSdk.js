import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";
import {setSelected} from "../../redux/reducers/orderSlice";
import {setAsset} from "../../redux/reducers/assetSlice";
import {scrollToTop} from "../../helpers/globals";
import SalePrice from "./SalePrice";
import {useDispatch} from "react-redux";
import SalePriceAsset from "./SalePriceAsset";

const AssetCard = ({asset}) => {

    const dispatch = useDispatch();
    return (
        <div className={'card asset_card__'}>

            <div className={'card-body'}>
                <p className={'text-muted text-right overflow-ellipsis'}>

                </p>

                <NavLink
                    to={`/recurso/${asset?.assetContract.address}/${asset?.tokenId}`}
                    onClick={() => {
                        dispatch(setSelected({
                                tokenId: asset?.tokenId,
                                assetContractAddress: asset?.assetContract.address
                            })
                        );
                        dispatch(setAsset(null));
                        scrollToTop();
                    }}
                >
                    <div
                        className={'asset_card__image'}
                        style={{
                            backgroundImage: `url(${asset?.imageUrl})`
                        }}
                    />
                </NavLink>
            </div>

            <div className={'card-body py-0'}>
                <div className={'card-title text-left'}>
                    <NavLink
                        to={`/recurso/${asset?.assetContract.address}/${asset?.tokeId}`}
                        onClick={() => {
                            dispatch(setSelected({
                                    tokenId: asset?.tokenId,
                                    assetContractAddress: asset?.assetContract.address
                                })
                            );
                        }}
                    >
                        <small className={'text-uppercase work-sans-light'}>
                            {asset?.collection.name}
                        </small>
                        <h5
                            className={'pb-3 overflow-ellipsis'}
                            title={asset?.name}
                        >
                            {asset?.name ? asset?.name : "-"}
                        </h5>
                    </NavLink>
                </div>
            </div>

            <div className={'card-body pt-0'}>
                <div className={'text-center d-flex justify-content-between text-medium-gray work-sans-bold'}>
                    <small>Quedan 18h</small>

                    <SalePriceAsset sellOrders={asset?.sellOrders}/>
                </div>
            </div>

        </div>
    );
};

AssetCard.propTypes = {
    
};

export default AssetCard;