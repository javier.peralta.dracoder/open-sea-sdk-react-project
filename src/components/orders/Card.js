import React from 'react';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import {setSelected} from "../../redux/reducers/orderSlice";
import {NavLink} from "react-router-dom";
import SalePrice from "./SalePrice";
import {scrollToTop} from "../../helpers/globals";
import {setAsset} from "../../redux/reducers/assetSlice";

const Card = ({order}) => {

    const dispatch = useDispatch();

    return (
        <div className={'card asset_card__'}>

            <div className={'card-body'}>
                <p className={'text-muted text-right overflow-ellipsis'}>

                </p>

                <NavLink
                    to={`/recurso/${order.asset?.assetContract.address}/${order.asset?.tokenId}`}
                    onClick={() => {
                        dispatch(setSelected({
                                tokenId: order.asset?.tokenId,
                                assetContractAddress: order.asset?.assetContract.address
                            })
                        );
                        dispatch(setAsset(null));
                        scrollToTop();
                    }}
                >
                    <div
                        className={'asset_card__image'}
                        style={{
                            backgroundImage: `url(${order.asset?.imageUrl})`
                        }}
                    />
                </NavLink>
            </div>

            <div className={'card-body py-0'}>
                <div className={'card-title text-left'}>
                    <NavLink
                        to={`/recurso/${order.asset?.assetContract.address}/${order.asset?.tokenId}`}
                        onClick={() => {
                            dispatch(setSelected({
                                tokenId: order.asset?.tokenId,
                                assetContractAddress: order.asset?.assetContract.address
                                })
                            );
                        }}
                    >
                        <small className={'text-uppercase work-sans-light'}>
                            {order.asset?.collection.name}
                        </small>
                        <h5
                            className={'pb-3 overflow-ellipsis'}
                            title={order.asset?.name}
                        >
                            {order.asset?.name ? order.asset?.name : "-"}
                        </h5>
                    </NavLink>
                </div>
            </div>

            <div className={'card-body pt-0'}>
                <div className={'text-center d-flex justify-content-between text-medium-gray work-sans-bold'}>
                    <small>Quedan 18h</small>
                    <SalePrice order={order}/>
                </div>
            </div>

        </div>
    );
};

Card.propTypes = {
    order: PropTypes.object.isRequired,
};

export default Card;