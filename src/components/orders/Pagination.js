import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {nextPage, prevPage} from "../../redux/reducers/paginationSlice";

const Pagination = () => {
    const dispatch = useDispatch();
    const {options} = useSelector(state => state.pagination);

    return (
        <div className={'col-12'}>
            <nav className={'my-3'}>
                <ul className="pagination justify-content-center">
                    <li>
                    <span className={`page-link btn ${options.page === 1 && 'disabled'}`}
                          onClick={() => {
                              dispatch(prevPage())
                          }}
                    >
                        <i className="fa fa-chevron-left"/> Anterior
                    </span>
                    </li>
                    <li>
                    <span className="page-link btn"
                          onClick={() => {
                              dispatch(nextPage())
                          }}
                    >
                        <i className="fa fa-chevron-right"/> Siguiente
                    </span>
                    </li>
                </ul>
            </nav>
        </div>
    )
};

export default Pagination;