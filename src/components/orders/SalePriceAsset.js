import React, {useEffect, useState} from 'react';
import {toUnitAmount} from "../../helpers/currency";
import {useSelector} from "react-redux";
import Skeleton from "react-loading-skeleton";

const SalePrice = ({sellOrders}) => {
    const {ethPrice} = useSelector(state => state.ethPrice);
    const [order, setOrder] = useState(sellOrders);
    const [priceLabel, setPriceLabel] = useState(0);

    useEffect(() => {
        if (sellOrders) {
            setOrder(sellOrders[0]);
            const {currentPrice, paymentTokenContract} = order;
            const price = toUnitAmount(currentPrice, paymentTokenContract);
            setPriceLabel(parseFloat(price).toLocaleString(undefined, {minimumSignificantDigits: 1}));
        }

    }, [sellOrders, ethPrice,order])

    return (
        <h5>
            {!ethPrice ? <Skeleton/> : (priceLabel * ethPrice.EUR).toFixed(2) + "€"}
        </h5>
    );
};

export default SalePrice;