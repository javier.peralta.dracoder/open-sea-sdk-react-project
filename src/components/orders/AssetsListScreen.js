import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import {setLoading, setTraits} from "../../redux/reducers/orderTraitsSlice";
import axios from "axios";
import {assetsUri, assetUri} from "../../helpers/openSeaApiUris";
import {setAsset} from "../../redux/reducers/assetSlice";
import {setAssets} from "../../redux/reducers/assetsSlice";
import {loadingList} from "../../helpers/globals";
import Swal from "sweetalert2";
import Pagination from "./Pagination";

const AssetsListScreen = props => {

    const dispatch = useDispatch();

    const {assets} = useSelector(state => state.assets);
    const {isLoading} = useSelector(state => state.loading);

    const getAssets = () => {
        dispatch(setLoading(true));
        return axios
            .get(`${assetsUri}?order_direction=desc&offset=0&limit=20`)
            .then((response) => {
                dispatch(setAssets(response.data.assets));
                dispatch(setLoading(false));
                console.log(response)

            })
            .catch(error => console.log('e', error));
    }

    useEffect(() => {
        getAssets();
    }, []);

    return (
        <>
            {assets?.length && assets?.length >= 20 ? <Pagination/> : null}

            <div className={'row mt-3 mb-5'}>
            </div>

            {isLoading === true ? loadingList() : Swal.close()}
        </>
    );
};

AssetsListScreen.propTypes = {};

export default AssetsListScreen;