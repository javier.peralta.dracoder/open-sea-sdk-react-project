import React, {useEffect, useState} from 'react';
import CardLoading from "./CardLoading";
import {useDispatch, useSelector} from "react-redux";
import {fetchCollections} from "../../helpers/collections/fetchCollections";
import {setAssets, setCategories, setLoading} from "../../redux/reducers/categoriesSlice";
import {assetsUri} from "../../helpers/openSeaApiUris";
import axios from "axios";
import AssetCard from "./AssetCard";
import {OrderSide} from "opensea-js/lib/types";
import {defaultOwner} from "../../helpers/constants";

const CategoryAssets = ({category}) => {
    const dispatch = useDispatch();
    // const {assets} = useSelector(state => state.categories);
    const [assets, setAssets] = useState(null);

    const getAssets = () => {
        setTimeout(function () {
            axios.get(`${assetsUri}`, {
                params: {
                    collection: category.slug,
                    owner: defaultOwner,
                    limit: 4,
                    side: OrderSide.Buy,
                }
            }).then(({data}) => {
                setAssets(data.assets);
            }).catch(error => {
                getAssets();
            })
        }, 2000);
    }

    useEffect(() => {
        getAssets();
    }, [dispatch, category]);

    return (
        <>
            {
                (!assets?.length && !assets) &&
                (
                    <div className={'row text-center'}>
                        <CardLoading count={4}/>
                    </div>
                )
            }
            {
                (assets && assets?.length) ?
                    (
                        <>
                            <h4 className={'text-center lora-regular my-4'} key={category.id}>{category.name}</h4>
                            <div className={'row text-center'}>
                                {
                                    assets?.map((asset, index) => {
                                        return (
                                            <div key={index} className={'p-1 col-12 col-sm-6 col-md-3 col-xl-3'}>
                                                <AssetCard asset={asset} key={index}/>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </>
                    ) : ''
            }

        </>
    );
};

export default CategoryAssets;