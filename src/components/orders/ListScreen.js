import React, {useState, useEffect} from 'react';

import {useDispatch, useSelector} from "react-redux";
import {setCount, setOptions} from "../../redux/reducers/paginationSlice";
import {orderProcessedAlert} from "../../data/strings";
import Toast from "../ui/toasts/Toast";
import {toast} from "react-toastify";
import {setTransactionHash} from "../../redux/reducers/orderSlice";
import {setLoading} from "../../redux/reducers/loadingSlice";
import Aside from "../ui/Aside";
import CardLoading from "./CardLoading";
import Banner from "../ui/Banner";
import CategoryAssets from "./categoryAssets";
import AssetCard from "./AssetCard";
import {fetchAssets} from "../../helpers/assets/fetchAssets";
import Pagination from "./Pagination";
import AssetCardSdk from "./AssetCardSdk";

const ListScreen = ({auction = false}) => {

    const dispatch = useDispatch();
    const {isLoading} = useSelector(state => state.loading);

    const {options} = useSelector(state => state.pagination);
    const {transactionHash} = useSelector(state => state.order);
    const {categories} = useSelector(state => state.categories);
    const [assets, setAssets] = useState(null);

    // dispatch(setOptions({is_english: auction}));

    const getAssets = () => {
        setAssets([]);
        setTimeout(function () {
            fetchAssets(options)
                .then(({assets}) => {
                    setAssets(assets);
                    dispatch(setLoading(false));
                })
                .catch(error => getAssets())

        }, 2000);
    }

    useEffect(() => {
        dispatch(setLoading(true));

        getAssets();

        if (transactionHash) {
            toast.success(orderProcessedAlert.message);

            dispatch(setTransactionHash(null));
        }

    }, [dispatch, transactionHash, auction, options])

    return (
        <>
            <Banner/>

            <div className={'col-12 mt-5'}>
                <div className={'row mt-3 mb-5'}>
                    <div className={'col-sm-2'}>
                        <Aside/>
                    </div>

                    <div className={'col-sm-10 justify-content-center'}>
                        <h4 className={'text-center lora-regular mb-4'}>Productos nuevos</h4>
                        <div className={'row'}>

                            {(assets?.length && assets?.length >= 20) || options.page > 1 ? <Pagination/> : null}
                            {
                                assets?.map((asset, index) => (
                                    <div key={index} className={'p-1 col-12 col-sm-6 col-md-3 col-xl-3'}>
                                        <AssetCardSdk key={index} asset={asset}/>
                                    </div>
                                ))
                            }

                            {isLoading === true && <CardLoading count={8}/>}

                            {(assets?.length && assets?.length >= 20) || options.page > 1 ? <Pagination/> : null}
                        </div>

                        {
                            categories?.map((category, index) => {
                                return (
                                    <CategoryAssets key={index} category={category}/>
                                )
                            })
                        }
                    </div>
                </div>

                <Toast/>
            </div>
        </>
    );
}

export default ListScreen;