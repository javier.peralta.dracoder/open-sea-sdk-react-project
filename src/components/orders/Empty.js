import React from 'react';

const Empty = () => {
    return (
        <>
            <div className={'row'}>
                <h2 className={'text-muted text-center'}>Ups... Parece que no tenemos recursos disponibles en éste
                    momento...</h2>
            </div>
        </>
    );
};

export default Empty;