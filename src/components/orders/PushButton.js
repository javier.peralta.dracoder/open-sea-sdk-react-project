import React from 'react';
import PropTypes from 'prop-types';
import {connectWallet} from "../../helpers/connectWallet";
import {seaport} from "../../helpers/constants";
import {accountAddress} from "../../helpers/globals";
import SalePrice from "./SalePrice";
import {useDispatch, useSelector} from "react-redux";
import {setProcessing, setSelected, setTransactionHash} from "../../redux/reducers/orderSlice";
import UsdPrice from "./UsdPrice";
import Units from "../order/Units";
import Swal from "sweetalert2";
import {orderProcessedAlert, orderProcessingAlert} from "../../data/strings";

const PushButton = ({order, history}) => {
    const {isProcessing} = useSelector((state) => state.order);
    const dispatch = useDispatch();

    const fulfillOrder = async (order, accountAddress) => {
        dispatch(setSelected(order.hash));
        Swal.fire({
            title: orderProcessingAlert.title,
            html: orderProcessingAlert.message,
            type: 'info',
            showConfirmButton: false,
            allowOutsideClick: false,
        });

        if (!accountAddress) {
            accountAddress = await connectWallet();
            const [accAdd] = accountAddress;

            dispatch(setProcessing(true))

            seaport.fulfillOrder({
                order: order, accountAddress: accAdd
            }).then(transactionHash => {
                dispatch(setTransactionHash(transactionHash));

                dispatch(setProcessing(false));

                Swal.fire({
                    title: orderProcessedAlert.title,
                    text: orderProcessedAlert.message,
                    type: 'success',
                    confirmButtonText: 'Entendido'
                }).then((result) => {
                    history.push('/mis-recursos');
                })
            }).catch(error => {
                Swal.fire({
                    title: 'Error',
                    text: error,
                    type: 'error',
                })
            });

        }
    }

    return (
        <>

            <button
                className={'btn btn-secondary h-50 py-2 w-25 mt-3'}
                onClick={() => {
                    fulfillOrder(order, accountAddress);
                }}
            >
                {isProcessing ? <i className='fa fa-spinner fa-spin'/> : "Pujar"}
            </button>

        </>
    );
};

PushButton.propTypes = {
    order: PropTypes.object.isRequired
};

export default PushButton;