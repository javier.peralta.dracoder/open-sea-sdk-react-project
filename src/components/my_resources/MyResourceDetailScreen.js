import React, {useCallback, useEffect} from 'react';
import {useParams} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {setLoading, setTraits} from "../../redux/reducers/orderTraitsSlice";
import axios from "axios";
import {assetUri} from "../../helpers/openSeaApiUris";
import {setAsset} from "../../redux/reducers/assetSlice";
import Swal from "sweetalert2";
import Traits from "../order/Traits";

const MyResourceDetailScreen = ({history}) => {
    const dispatch = useDispatch();
    const {tokenAddress, tokenId} = useParams();
    const {traits, isLoading} = useSelector(state => state.orderTraits);
    const {asset} = useSelector(state => state.asset);

    if (!tokenAddress || !tokenId) {
        history.goBack();
    }
    const getAsset = useCallback((assetContractAddress, tokenId) => {
        dispatch(setLoading(true));

        setTimeout(function () {
            return axios
                .get(`${assetUri}/${assetContractAddress}/${tokenId}`)
                .then(({data}) => {
                    dispatch(setAsset(data));
                    dispatch(setTraits(data.traits));
                    dispatch(setLoading(false));
                    console.log(asset)
                })
                .catch(error => console.log('e', error));
        }, 2000);
    }, [asset, dispatch])

    const videoModal = (video) => {
        Swal.fire({
            width: '80vw',
            html: `<video  loop autoPlay='' width="100%">
                    <source src="${video}" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>`,
            confirmButtonText: 'Cerrar',
        })
    }

    const renderVideo = (video) => {
        return (
            <>
                <video loop autoPlay={true} width={'100%'} className={'btn'} muted
                       onClick={() => {
                           videoModal(video);
                       }}
                >
                    <source src={asset?.animation_original_url} type="video/mp4"/>
                </video>
            </>
        )

    }

    const imageModal = (imageUrl) => {
        Swal.fire({
            title: asset?.name,
            text: asset?.description,
            imageUrl,
            confirmButtonText: 'Cerrar',
        });
    }

    const unlockableContentalert = () => {
        Swal.fire({
            title: 'Contenido desbloqueable',
            html: '<p><b>Solamente el dueño de éste recurso puede visualizar el contenido desbloqueable.</b></p>',
            confirmButtonText: '¡Entendido!'
        })
    }

    useEffect(() => {
        dispatch(setAsset({}));
        dispatch(setTraits([]));
        getAsset(tokenAddress, tokenId);
    }, [dispatch, getAsset, tokenAddress, tokenId]);

    return (
        <>
            <div className={'row mt-4'}>
                <div className={'text-lef'}>
                    <button
                        type={"button"}
                        className={'btn btn-secondary btn-sm'}
                        onClick={() => {
                            history.goBack()
                        }}
                    >
                        <i className={'fas fa-arrow-left'}/>
                        &emsp;Volver
                    </button>
                </div>
            </div>

            <div className={'row mt-4'}>
                <div className={'col-6 text-center img-thumbnail'}>
                    {
                        asset?.animation_original_url ?
                            renderVideo(asset?.animation_original_url)
                            :
                            (
                                <img src={asset?.image_original_url} alt={asset?.name}
                                     className={'img-fluid btn asset_detail__image'}
                                     onClick={() => {
                                         imageModal(asset?.image_original_url);
                                     }}
                                />
                            )
                    }
                </div>

                <div className={'col-6'}>
                    <h1 className={'border-bottom pb-3 mb-3'}>{asset?.name}</h1>
                    <article>
                        <p>{asset?.description}</p>
                    </article>

                    <article>
                        {isLoading && <p><i className={'fas fa-spinner fa-spin'}/>&emsp;Cargando características</p>}
                        <Traits traits={traits}/>
                    </article>

                    <div
                        className={'alert alert-info btn btn-block'}
                        onClick={() => {
                            unlockableContentalert();
                        }}
                    >
                        <p className={'text-center m-0'}><i className={'fas fa-lock'}/>&emsp;<b>Incluye contenido
                            desbloqueable</b></p>
                    </div>
                </div>
            </div>
        </>
    );
};

export default MyResourceDetailScreen;