import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";

const AssetCard = ({asset}) => {
    return (
        <div className={'card'}>

            <div className={'card-body'}>
                <p className={'text-muted text-right overflow-ellipsis'}>
                    <small>
                        {asset?.collection.name}
                    </small>
                </p>
                <NavLink to={`/mis-recursos/${asset?.tokenAddress}/${asset?.tokenId}`}>
                    <div
                        className={'asset_card__image'}
                        style={{
                            backgroundImage: `url(${asset?.imageUrl})`
                        }}
                    />
                </NavLink>
            </div>

            <div className={'card-body py-0'}>
                <div className={'card-title text-center'}>
                    <NavLink to={`/mis-recursos/${asset?.tokenAddress}/${asset?.tokenId}`}>
                        <h5
                            className={'border-bottom pb-3 overflow-ellipsis'}
                            title={asset?.name}
                        >
                            {asset?.name ? asset?.name : "-"}
                        </h5>
                    </NavLink>
                    <article className={'asset_card__description'}>
                        {
                            asset?.description &&
                            <p className="card-text text-left">
                                {asset?.description.substr(0, 100)}{asset?.description.length > 100 && '...'}
                            </p>
                        }
                    </article>
                </div>
            </div>

            <div className={'card-body pt-0'}>

            </div>

        </div>
    );
};

AssetCard.propTypes = {
    asset: PropTypes.object.isRequired,
};

export default AssetCard;