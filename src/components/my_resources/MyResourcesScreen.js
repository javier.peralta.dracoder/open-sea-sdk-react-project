import React, {useEffect, useState} from 'react';
import {setLoading} from "../../redux/reducers/loadingSlice";
import {setCount} from "../../redux/reducers/paginationSlice";
import {loadingList, scrollToTop, throttleError} from "../../helpers/globals";
import {useDispatch, useSelector} from "react-redux";
import {fetchAssets} from "../../helpers/assets/fetchAssets";
import AssetCard from "./AssetCard";
import Swal from "sweetalert2";
import {connectWallet} from "../../helpers/connectWallet";
import {setOwner} from "../../redux/reducers/myOrdersSlice";
import Pagination from "../orders/Pagination";

const MyResourcesScreen = () => {
    const dispatch = useDispatch();
    const [assets, setAssets] = useState(null);
    const {isLoading} = useSelector(state => state.loading);
    const {owner} = useSelector(state => state.myOrders)

    useEffect(() => {
        connectWallet().then(walletResponse => {

            dispatch(setLoading(true));
            dispatch(setOwner(walletResponse[0]));

            if (walletResponse[0] && owner) {
                fetchAssets({owner})
                    .then(({assets, count}) => {
                        setAssets(assets);
                        dispatch(setCount(count));
                        dispatch(setLoading(false));
                        scrollToTop();
                    })
                    .catch(() => {
                        throttleError();
                    });
            }
        });

    }, [owner, dispatch])
    return (
        <section className={'container-fluid py-4'}>
            <h1 className={'text-center border-bottom pb-3'}>Mis recursos</h1>
            {assets?.length && assets.length >= 20 ? <Pagination/> : null}
            <div className={'row mt-3 mb-5'}>
                {
                    assets?.map((asset, index) => (
                        <div key={index} className={'p-1 col-12 col-sm-6 col-md-3 col-xl-2'}>
                            <AssetCard key={index} asset={asset}/>
                        </div>
                    ))
                }

            </div>
            {assets?.length && assets.length >= 20 ? <Pagination/> : null}

            {isLoading === true ? loadingList() : Swal.close()}
        </section>
    );
};


export default MyResourcesScreen;